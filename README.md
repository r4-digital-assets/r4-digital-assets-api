## Micronaut 3.1.4 Documentation

- [User Guide](https://docs.micronaut.io/3.1.4/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.1.4/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.1.4/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

## AutoGenerate liquibase changelogs
1. Go to liquibase.gradle file and set changeLogFile property to file where changelog will be generated. `changeLogFile 'out/changelog_account_temp.yml'`
2. Go to liquibase.gradle file and set url property to package that include entities. `url 'hibernate:spring:io.dcts.accountXXXXXXXXXXXXXX`
3. Execute gradlew tasks that generate the result file into out directory. `./gradlew compileGroovy generateChangelog`

Target folder (out in this example) must exists.


## Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)
